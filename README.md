# vmio8: A chip-8 emulator written in C++

![Space Invaders](./assets/space_invaders.png)

## IMPORTANT
This project is not finished and there are no guarantees that the program will
run as intended. This notice will be removed once it is implemented and works
as it should.

## Dependencies
- SDL2

## Building
### Release build
```shell
$ make release
```
### Debug build
```shell
$ make debug
```

## Running
Running vmio8 with a file called `romfile.ch8`, scaling the video to `10` and
having a cycle delay of `1`:

```shell
$ ./vmio8 10 1 romfile.ch8
```

## Todo
- [X] Implement the DXYN instruction
- [X] Fix bug in `Platform::update`
- [X] Fix bug in `Emulator::_00ee`
- [ ] Implement sound
