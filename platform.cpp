#include "platform.hpp"
#include <stdexcept>

namespace Vmio8
{
    Platform::Platform(const char *title,
                       int window_width,
                       int window_height,
                       int texture_width,
                       int texture_height)
        : window{},
          renderer{},
          texture{}
    {
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            throw std::runtime_error{"Failed to initialize SDL: " + std::string{SDL_GetError()}};
        }

        if (window = SDL_CreateWindow(title,
                                      0, 0,
                                      window_width, window_height,
                                      SDL_WINDOW_SHOWN);
            !window)
        {
            throw std::runtime_error{"Failed to initialize window: " + std::string{SDL_GetError()}};
        }

        if (renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED); !renderer)
        {
            throw std::runtime_error{"Failed to initialize renderer: " + std::string{SDL_GetError()}};
        }

        if (texture = SDL_CreateTexture(renderer,
                                        SDL_PIXELFORMAT_RGBA8888,
                                        SDL_TEXTUREACCESS_STREAMING,
                                        texture_width, texture_height);
            !texture)
        {
            throw std::runtime_error{"Failed to initialize texture: " + std::string{SDL_GetError()}};
        }
    }

    Platform::~Platform()
    {
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    bool Platform::process_input(Byte *keypad) const
    {
        bool quit{false};
        SDL_Event event{};

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
            {
                quit = true;
            }
            break;

            case SDL_KEYDOWN:
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                {
                    quit = true;
                }
                break;

                case SDLK_x:
                {
                    keypad[0] = KEY_DOWN;
                }
                break;

                case SDLK_1:
                {
                    keypad[1] = KEY_DOWN;
                }
                break;

                case SDLK_2:
                {
                    keypad[2] = KEY_DOWN;
                }
                break;

                case SDLK_3:
                {
                    keypad[3] = KEY_DOWN;
                }
                break;

                case SDLK_q:
                {
                    keypad[4] = KEY_DOWN;
                }
                break;

                case SDLK_w:
                {
                    keypad[5] = KEY_DOWN;
                }
                break;

                case SDLK_e:
                {
                    keypad[6] = KEY_DOWN;
                }
                break;

                case SDLK_a:
                {
                    keypad[7] = KEY_DOWN;
                }
                break;

                case SDLK_s:
                {
                    keypad[8] = KEY_DOWN;
                }
                break;

                case SDLK_d:
                {
                    keypad[9] = KEY_DOWN;
                }
                break;

                case SDLK_z:
                {
                    keypad[10] = KEY_DOWN;
                }
                break;

                case SDLK_c:
                {
                    keypad[11] = KEY_DOWN;
                }
                break;

                case SDLK_4:
                {
                    keypad[12] = KEY_DOWN;
                }
                break;

                case SDLK_r:
                {
                    keypad[13] = KEY_DOWN;
                }
                break;

                case SDLK_f:
                {
                    keypad[14] = KEY_DOWN;
                }
                break;

                case SDLK_v:
                {
                    keypad[15] = KEY_DOWN;
                }
                break;

                default:
                {
                }
                break;
                }
            }
            break;

            case SDL_KEYUP:
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_x:
                {
                    keypad[0] = KEY_UP;
                }
                break;

                case SDLK_1:
                {
                    keypad[1] = KEY_UP;
                }
                break;

                case SDLK_2:
                {
                    keypad[2] = KEY_UP;
                }
                break;

                case SDLK_3:
                {
                    keypad[3] = KEY_UP;
                }
                break;

                case SDLK_q:
                {
                    keypad[4] = KEY_UP;
                }
                break;

                case SDLK_w:
                {
                    keypad[5] = KEY_UP;
                }
                break;

                case SDLK_e:
                {
                    keypad[6] = KEY_UP;
                }
                break;

                case SDLK_a:
                {
                    keypad[7] = KEY_UP;
                }
                break;

                case SDLK_s:
                {
                    keypad[8] = KEY_UP;
                }
                break;

                case SDLK_d:
                {
                    keypad[9] = KEY_UP;
                }
                break;

                case SDLK_z:
                {
                    keypad[10] = KEY_UP;
                }
                break;

                case SDLK_c:
                {
                    keypad[11] = KEY_UP;
                }
                break;

                case SDLK_4:
                {
                    keypad[12] = KEY_UP;
                }
                break;

                case SDLK_r:
                {
                    keypad[13] = KEY_UP;
                }
                break;

                case SDLK_f:
                {
                    keypad[14] = KEY_UP;
                }
                break;

                case SDLK_v:
                {
                    keypad[15] = KEY_UP;
                }
                break;

                default:
                {
                }
                break;
                }
            }
            break;

            default:
            {
            }
            break;
            }
        }

        return quit;
    }

    void Platform::update(SharedDWordArrPtr frame_buf, int pitch)
    {
        if (SDL_UpdateTexture(texture, nullptr, frame_buf.get(), pitch) < 0)
        {
            throw std::runtime_error{"Failed to update texture: " + std::string{SDL_GetError()}};
        }

        if (SDL_RenderClear(renderer) < 0)
        {

            throw std::runtime_error{"Failed to clear renderer: " + std::string{SDL_GetError()}};
        }

        if (SDL_RenderCopy(renderer, texture, nullptr, nullptr) < 0)
        {
            throw std::runtime_error{"Failed to copy renderer: " + std::string{SDL_GetError()}};
        }

        SDL_RenderPresent(renderer);
    }
}