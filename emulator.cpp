#include "emulator.hpp"
#include "util.hpp"
#include <stdexcept>
#include <ctime>

namespace Vmio8
{
    Emulator::Emulator(const char *program)
        : regs{},
          opcode{},
          memory{new Byte[MEMORY_SIZE]{0}},
          idx{},
          pc{PROGRAM_START},
          frame_buf{new DWord[FRAME_BUF_SIZE]{0}},
          delay_timer{},
          sound_timer{},
          stack{},
          sp{},
          keypad{}
    {
        // Use current time as seed for random generator (instruction CXNN)
        std::srand(std::time(nullptr));

        // Load FONTSET
        for (auto i{0}; i < FONTSET_SIZE; i++)
        {
            memory[i + FONTSET_START] = FONTSET[i];
        }

        if (auto *fp{std::fopen(program, "r")}; fp)
        {
            fseek(fp, 0, SEEK_END);
            auto file_size{ftell(fp)};
            rewind(fp);

            if (file_size > (MEMORY_SIZE - PROGRAM_START))
            {
                throw std::runtime_error{"ERROR: Cannot read " + std::string{program} + " into memory; too big"};
            }

            // Load program
            for (auto i{0}; i < file_size; i++)
            {
                if (!std::fread(&memory[i + PROGRAM_START], 1, 1, fp))
                {
                    break;
                }
            }
        }
        else
        {
            throw std::runtime_error{"ERROR: Failed to open specified program: " + std::string{program}};
        }
    }

    const SharedDWordArrPtr Emulator::get_frame_buf() const
    {
        return frame_buf;
    }

    Byte *Emulator::get_keypad()
    {
        return keypad;
    }

    const Word Emulator::fetch() const
    {
        return (memory[pc] << 8) | memory[pc + 1];
    }

    void Emulator::execute()
    {
        // fetch opcode
        opcode = fetch();
        // increment program counter
        pc += 2;

        switch (opcode & 0xf000)
        {
        case 0x0000:
        {
            switch (opcode & 0x00ff)
            {
            // 00e0 - CLS
            case 0x00e0:
            {
                _00e0();
            }
            break;
            // 00ee - RET
            case 0x00ee:
            {
                _00ee();
            }
            break;
            // 0nnn - SYS addr
            // Instruction 0x0nnn used on old computers on which chip8 was implemented,
            // modern interpreters ignore it
            default:
            {
            }
            break;
            }
        }
        break;
        // 1nnn - JP addr
        case 0x1000:
        {
            _1nnn(opcode & 0x0fff);
        }
        break;
        // 2nnn - CALL addr
        case 0x2000:
        {
            _2nnn(opcode & 0x0fff);
        }
        break;
        // 3xkk - SE Vx, byte
        case 0x3000:
        {
            _3xkk((opcode & 0x0f00) >> 8, opcode & 0x00ff);
        }
        break;
        // 4xkk - SNE Vx, byte
        case 0x4000:
        {
            _4xkk((opcode & 0x0f00) >> 8, opcode & 0x00ff);
        }
        break;
        // 5xy0 - SE Vx, Vy
        case 0x5000:
        {
            _5xy0((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
        }
        break;
        // 6xkk - LD Vx, byte
        case 0x6000:
        {
            _6xkk((opcode & 0x0f00) >> 8, opcode & 0x00ff);
        }
        break;
        // 7xkk - Add Vx, byte
        case 0x7000:
        {
            _7xkk((opcode & 0x0f00) >> 8, opcode & 0x00ff);
        }
        break;
        case 0x8000:
        {
            switch (opcode & 0x000f)
            {
            // 8xy0 - LD Vx, Vy
            case 0x0000:
            {
                _8xy0((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy1 - OR Vx, Vy
            case 0x0001:
            {
                _8xy1((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy2 - AND Vx, Vy
            case 0x0002:
            {
                _8xy2((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy3 - XOR Vx, Vy
            case 0x0003:
            {
                _8xy3((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy4 - ADD Vx, Vy
            case 0x0004:
            {
                _8xy4((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy5 - SUB Vx, Vy
            case 0x0005:
            {
                _8xy5((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xy6 - SHR Vx, {, Vy}
            case 0x0006:
            {
                _8xy6((opcode & 0x0f00) >> 8);
            }
            break;
            // 8xy7 - SUBN Vx, Vy
            case 0x0007:
            {
                _8xy7((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
            }
            break;
            // 8xye - SHL Vx, {, Vy}
            case 0x000e:
            {
                _8xye((opcode & 0x0f00) >> 8);
            }
            break;
            default:
            {
                throw std::runtime_error{"ERROR: Unexpected opcode " + std::to_string(opcode)};
            }
            break;
            }
        }
        break;
        // 9xy0 - SNE Vx, Vy
        case 0x9000:
        {
            _9xy0((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4);
        }
        break;
        // annn - LD I, addr
        case 0xa000:
        {
            _annn(opcode & 0x0fff);
        }
        break;
        // bnnn - JP V0, addr
        case 0xb000:
        {
            _bnnn(opcode & 0x0fff);
        }
        break;
        // cxkk - RND Vx, byte
        case 0xc000:
        {
            _cxkk((opcode & 0x0f00) >> 8, opcode & 0x00ff);
        }
        break;
        // dxyn - DRW Vx, Vy, nibble
        case 0xd000:
        {
            _dxyn((opcode & 0x0f00) >> 8, (opcode & 0x00f0) >> 4, opcode & 0x000f);
        }
        break;
        case 0xe000:
        {
            switch (opcode & 0x00ff)
            {
            // ex9e - SKP Vx
            case 0x009e:
            {
                _ex9e((opcode & 0x0f00) >> 8);
            }
            break;
            // exa1 - SKNP Vx
            case 0x00a1:
            {
                _exa1((opcode & 0x0f00) >> 8);
            }
            break;
            default:
            {
                throw std::runtime_error{"ERROR: Unexpected opcode " + std::to_string(opcode)};
            }
            break;
            }
        }
        break;
        case 0xf000:
        {
            switch (opcode & 0x00ff)
            {
            // fx07 - LD Vx, DT
            case 0x0007:
            {
                _fx07((opcode & 0x0f00) >> 8);
            }
            break;
            // fx0a - LD Vx, K
            case 0x000a:
            {
                _fx0a((opcode & 0x0f00) >> 8);
            }
            break;
            // fx15 - LD DT, Vx
            case 0x0015:
            {
                _fx15((opcode & 0x0f00) >> 8);
            }
            break;
            // fx18 - LD ST, Vx
            case 0x0018:
            {
                _fx18((opcode & 0x0f00) >> 8);
            }
            break;
            // fx1e - ADD I, Vx
            case 0x001e:
            {
                _fx1e((opcode & 0x0f00) >> 8);
            }
            break;
            // fx29 - LD F, Vx
            case 0x0029:
            {
                _fx29((opcode & 0x0f00) >> 8);
            }
            break;
            // fx33 - LD B, Vx
            case 0x0033:
            {
                _fx33((opcode & 0x0f00) >> 8);
            }
            break;
            // fx55 - LD [I], Vx
            case 0x0055:
            {
                _fx55((opcode & 0x0f00) >> 8);
            }
            break;
            // fx65 - LD Vx, [I]
            case 0x0065:
            {
                _fx65((opcode & 0x0f00) >> 8);
            }
            break;
            }
        }
        break;
        default:
        {
            throw std::runtime_error{"ERROR: Unexpected opcode " + std::to_string(opcode)};
        }
        break;
        }

        if (delay_timer > 0)
        {
            delay_timer--;
        }

        if (sound_timer > 0)
        {
            sound_timer--;
        }
    }

    // Chip instructions

    // CLS
    // Clear the display.
    void Emulator::_00e0()
    {
        for (auto i{0}; i < FRAME_BUF_SIZE; i++)
        {
            frame_buf[i] = 0;
        }
    }

    // RET
    // Return from a subroutine. The interpreter sets the program counter to the address at the top
    // of the stack,then subtracts 1 from the stack pointer.
    void Emulator::_00ee()
    {
        pc = stack[--sp];
    }

    // JP addr
    // Jump to location nnn. The interpreter sets the program counter to nnn.
    void Emulator::_1nnn(Word addr)
    {
        pc = addr;
    }

    // CALL addr
    // Call subroutine at nnn. The interpreter increments the stack pointer, then puts the
    // current PC on the top of the stack. The PC is then set to nnn.
    void Emulator::_2nnn(Word addr)
    {
        stack[sp++] = pc;
        pc = addr;
    }

    // SE Vx, byte
    // Skip  next  instruction  if  Vx  =  kk. The  interpreter  compares  register  Vx  to  kk,
    // and  if  they  are  equal, increments the program counter by 2.
    void Emulator::_3xkk(Byte reg_x, Byte byte)
    {
        if (regs[reg_x] == byte)
            pc += 2;
    }

    // SNE Vx, byte
    // Skip next instruction if Vx != kk.  The interpreter compares register Vx to kk, and if they
    // are not equal,increments the program counter by 2.
    void Emulator::_4xkk(Byte reg_x, Byte byte)
    {
        if (regs[reg_x] != byte)
            pc += 2;
    }

    // SE Vx, Vy
    // Skip next instruction if Vx = Vy. The interpreter compares register Vx to register Vy, and
    // if they are equal,increments the program counter by 2.
    void Emulator::_5xy0(Byte reg_x, Byte reg_y)
    {
        if (regs[reg_x] == regs[reg_y])
            pc += 2;
    }

    // LD Vx, byte
    // Set Vx = kk. The interpreter puts the value kk into register Vx.
    void Emulator::_6xkk(Byte reg_x, Byte byte)
    {
        regs[reg_x] = byte;
    }

    // ADD Vx, byte
    // Set Vx = Vx + kk. Adds the value kk to the value of register Vx, then stores the result in
    // Vx.
    void Emulator::_7xkk(Byte reg_x, Byte byte)
    {
        regs[reg_x] += byte;
    }

    // LD Vx, Vy
    // Set Vx = Vy. Stores the value of register Vy in register Vx
    void Emulator::_8xy0(Byte reg_x, Byte reg_y)
    {
        regs[reg_x] = regs[reg_y];
    }

    // OR Vx, Vy
    // Set Vx = Vx OR Vy. Performs a bitwise OR on the values of Vx and Vy, then stores the
    // result in Vx. A bitwise OR compares the corresponding bits from two values, and if either
    // bit is 1, then the same bit in the result is also 1. Otherwise, it is 0.
    void Emulator::_8xy1(Byte reg_x, Byte reg_y)
    {
        regs[reg_x] |= regs[reg_y];
    }

    // AND Vx, Vy
    // Set Vx = Vx AND Vy. Performs a bitwise AND on the values of Vx and Vy, then stores the
    // result in Vx.A bitwise AND compares the corresponding bits from two values, and if both
    // bits are 1, then the same bitin the result is also 1. Otherwise, it is 0.
    void Emulator::_8xy2(Byte reg_x, Byte reg_y)
    {
        regs[reg_x] &= regs[reg_y];
    }

    // XOR Vx, Vy
    // Set Vx = Vx XOR Vy.  Performs a bitwise exclusive OR on the values of Vx and Vy, then
    // stores the resultin Vx.  An exclusive OR compares the corresponding bits from two values,
    // and if the bits are not both thesame, then the corresponding bit in the result is set to 1.
    // Otherwise, it is 0.
    void Emulator::_8xy3(Byte reg_x, Byte reg_y)
    {
        regs[reg_x] ^= regs[reg_y];
    }

    // ADD Vx, Vy
    // Set Vx = Vx + Vy, set 0xf = carry. The values of Vx and Vy are added together. If the
    // result is greater than 8 bits (i.e., 255,) 0xf is set to 1, otherwise 0. Only the lowest 8
    // bits of the result are kept, and stored in Vx.
    void Emulator::_8xy4(Byte reg_x, Byte reg_y)
    {
        // Overflow
        if ((0xff - regs[reg_x]) < regs[reg_y])
        {
            regs[0xf] = 1;
        }
        else
        {
            regs[0xf] = 0;
        }

        regs[reg_x] = (regs[reg_x] + regs[reg_y]) & 0x00ff;
    }

    // SUB Vx, Vy
    // Set  Vx  =  Vx  -  Vy,  set  0xf  =  NOT  borrow.   If  Vx  >  Vy,  then  0xf  is  set  to
    // 1,  otherwise  0.   Then  Vy  issubtracted from Vx, and the results stored in Vx.
    void Emulator::_8xy5(Byte reg_x, Byte reg_y)
    {
        // Underflow
        if (regs[reg_x] < regs[reg_y])
        {
            regs[0xf] = 0;
        }
        else
        {
            regs[0xf] = 1;
        }

        regs[reg_x] -= regs[reg_y];
    }

    // SHR Vx{, Vy}
    // Set Vx = Vx SHR 1. If the least-significant bit of Vx is 1, then 0xf is set to 1, otherwise
    // 0. Then Vx is divided by 2.
    void Emulator::_8xy6(Byte reg_x)
    {
        regs[0xf] = regs[reg_x] & 0x1;
        regs[reg_x] >>= 1;
    }

    // SUBN Vx, Vy
    // Set  Vx  =  Vy  -  Vx,  set  0xf  =  NOT  borrow.   If  Vy  >  Vx,  then  0xf  is  set  to
    // 1,  otherwise  0.   Then  Vx  is subtracted from Vy, and the results stored in Vx
    void Emulator::_8xy7(Byte reg_x, Byte reg_y)
    {
        // Underflow
        if (regs[reg_y] < regs[reg_x])
        {
            regs[0xf] = 0;
        }
        else
        {
            regs[0xf] = 1;
        }

        regs[reg_x] = regs[reg_y] - regs[reg_x];
    }

    // SHL Vx{, Vy}
    // Set Vx = Vx SHL 1. If the most-significant bit of Vx is 1, then 0xf is set to 1, otherwise
    // to 0. Then Vx is multiplied by 2.
    void Emulator::_8xye(Byte reg_x)
    {
        regs[0xf] = (regs[reg_x] & 0xff) >> 7;
        regs[reg_x] <<= 1;
    }

    // SNE Vx, Vy
    // Skip next instruction if Vx != Vy.  The values of Vx and Vy are compared, and if they are
    // not equal, theprogram counter is increased by 2.
    void Emulator::_9xy0(Byte reg_x, Byte reg_y)
    {
        if (regs[reg_x] != regs[reg_y])
            pc += 2;
    }

    // LD I, addr
    // Set I = nnn. The value of register I is set to nnn.
    void Emulator::_annn(Word addr)
    {
        idx = addr;
    }

    // JP V0, addr
    // Jump to location nnn + V0. The program counter is set to nnn plus the value of V0.
    void Emulator::_bnnn(Word addr)
    {
        pc = regs[0x0] + addr;
    }

    // RND Vx, byte
    // Set Vx = random byte AND kk. The interpreter generates a random number from 0 to 255,
    // which is thenANDed with the value kk. The results are stored in Vx. See instruction 8xy2
    // for more information on AND.
    void Emulator::_cxkk(Byte reg_x, Byte byte)
    {
        regs[reg_x] = (std::rand() % 255) & byte;
    }

    // DRW Vx, Vy, nibble
    // Display n-byte sprite starting at memory location I at (Vx, Vy), set 0xf = collision. The
    // interpreter reads nbytes from memory, starting at the address stored in I. These bytes are
    // then displayed as sprites on screenat coordinates (Vx, Vy). Sprites are XOR’d onto the
    // existing screen. If this causes any pixels to be erased,0xf is set to 1, otherwise it is
    // set to 0. If the sprite is positioned so part of it is outside the coordinates of the
    // display, it wraps around to the opposite side of the screen
    void Emulator::_dxyn(Byte reg_x, Byte reg_y, Byte nibble)
    {
        // Wrap around if at frame boundary
        auto x_pos{regs[reg_x] % FRAME_WIDTH};
        auto y_pos{regs[reg_y] % FRAME_HEIGHT};

        regs[0xf] = 0;

        for (auto row{0}; row < nibble; row++)
        {
            for (auto col{0}; col < 8; col++)
            {
                if (memory[row + idx] & (0x80 >> col)) // if pixel is on
                {
                    DWord *frame_pixel{&frame_buf[(y_pos + row) * FRAME_WIDTH + (x_pos + col)]};

                    if (*frame_pixel == PIXEL_ON)
                    {
                        regs[0xf] = 1;
                    }

                    *frame_pixel ^= PIXEL_ON;
                }
            }
        }
    }

    // SKP Vx
    // Skip next instruction if key with the value of Vx is pressed. Checks the keyboard, and if
    // the key correspondingto the value of Vx is currently in the down position, PC is increased
    // by 2.
    void Emulator::_ex9e(Byte reg_x)
    {
        if (keypad[regs[reg_x]] == KEY_DOWN)
            pc += 2;
    }

    // SKNP Vx
    // Skip  next  instruction  if  key  with  the  value  of  Vx  is  not  pressed.   Checks  the
    // keyboard,  and  if  the  keycorresponding to the value of Vx is currently in the up
    // position, PC is increased by 2.
    void Emulator::_exa1(Byte reg_x)
    {
        if (keypad[regs[reg_x]] == KEY_UP)
            pc += 2;
    }

    // LD Vx, DT
    // Set Vx = delay timer value. The value of DT is placed into Vx.
    void Emulator::_fx07(Byte reg_x)
    {
        regs[reg_x] = delay_timer;
    }

    // LD Vx, K
    // Wait for a key press, store the value of the key in Vx.  All execution stops until a key is
    // pressed, then thevalue of that key is stored in Vx.
    void Emulator::_fx0a(Byte reg_x)
    {
        bool key_press_found{false};

        for (auto i{0}; i < KEYPAD_SIZE; i++)
        {
            if (keypad[i] == KEY_DOWN)
            {
                regs[reg_x] = i;
                key_press_found = true;
                break;
            }
        }

        if (!key_press_found)
        {
            pc -= 2;
        }
    }

    // LD DT, Vx
    // Set delay timer = Vx. Delay Timer is set equal to the value of Vx.
    void Emulator::_fx15(Byte reg_x)
    {
        delay_timer = regs[reg_x];
    }

    // LD ST, Vx
    // Set sound timer = Vx. Sound Timer is set equal to the value of Vx.
    void Emulator::_fx18(Byte reg_x)
    {
        sound_timer = regs[reg_x];
    }

    // ADD I, Vx
    // Set I = I + Vx. The values of I and Vx are added, and the results are stored in I.
    void Emulator::_fx1e(Byte reg_x)
    {
        idx += regs[reg_x];
    }

    // LD F, Vx
    // Set  I  =  location  of  sprite  for  digit  Vx.   The  value  of  I  is  set  to  the
    // location  for  the  hexadecimal  spritecorresponding to the value of Vx.  See section 2.4,
    // Display, for more information on the Chip-8 hexadecimalfont.  To obtain this value,
    // multiply VX by 5 (all font data stored in first 80 bytes of memory).
    void Emulator::_fx29(Byte reg_x)
    {
        regs[reg_x] = (regs[reg_x] * 5) + FONTSET_START;
    }

    // LD B, Vx
    // Store BCD representation of Vx in memory locations I, I+1, and I+2.  The interpreter takes
    // the decimalvalue of Vx, and places the hundreds digit in memory at location in I, the tens
    // digit at location I+1, andthe ones digit at location I+2.
    void Emulator::_fx33(Byte reg_x)
    {
        memory[idx] = regs[reg_x] / 100;
        memory[idx + 1] = (regs[reg_x] % 100) / 10;
        memory[idx + 2] = regs[reg_x] % 10;
    }

    // LD [I], Vx
    // Stores 0 to VX in memory starting at address I. I is then set to I + x + 1.
    void Emulator::_fx55(Byte reg_x)
    {
        for (auto i{0x0}; i <= reg_x; i++)
        {
            memory[i + idx] = regs[i];
        }

        idx += reg_x + 1;
    }

    // LD Vx, [I]
    // Fills 0 to VX with values from memory starting at address I. I is then set to I + x + 1.
    void Emulator::_fx65(Byte reg_x)
    {
        for (auto i{0x0}; i <= reg_x; i++)
        {
            regs[i] = memory[i + idx];
        }

        idx += reg_x + 1;
    }
}