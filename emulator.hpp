#pragma once

#include <cstdio>
#include <cstdint>
#include <memory>

namespace Vmio8
{
    using Byte = uint8_t;
    using Word = uint16_t;
    using DWord = uint32_t;
    using UniqueByteArrPtr = std::unique_ptr<Byte[]>;
    using SharedDWordArrPtr = std::shared_ptr<DWord[]>;

    constexpr Word PROGRAM_START{0x200};
    constexpr Byte FONTSET_START{0x50};
    constexpr Byte FONTSET_SIZE{80};
    constexpr Byte FRAME_WIDTH{64};
    constexpr Byte FRAME_HEIGHT{32};
    constexpr Word MEMORY_SIZE{4096};
    constexpr Word FRAME_BUF_SIZE{FRAME_WIDTH * FRAME_HEIGHT};
    constexpr Byte STACK_SIZE{16};
    constexpr Byte KEYPAD_SIZE{16};
    constexpr Byte REGISTER_SIZE{16};
    constexpr Byte KEY_DOWN{0x0};
    constexpr Byte KEY_UP{0xff};
    constexpr DWord PIXEL_ON{0xffffffff};
    constexpr Byte FONTSET[FONTSET_SIZE]{
        0xf0, 0x90, 0x90, 0x90, 0xf0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xf0, 0x10, 0xf0, 0x80, 0xf0, // 2
        0xf0, 0x10, 0xf0, 0x10, 0xf0, // 3
        0x90, 0x90, 0xf0, 0x10, 0x10, // 4
        0xf0, 0x80, 0xf0, 0x10, 0xf0, // 5
        0xf0, 0x80, 0xf0, 0x90, 0xf0, // 6
        0xf0, 0x10, 0x20, 0x40, 0x40, // 7
        0xf0, 0x90, 0xf0, 0x90, 0xf0, // 8
        0xf0, 0x90, 0xf0, 0x10, 0xf0, // 9
        0xf0, 0x90, 0xf0, 0x90, 0x90, // A
        0xe0, 0x90, 0xe0, 0x90, 0xe0, // B
        0xf0, 0x80, 0x80, 0x80, 0xf0, // C
        0xe0, 0x90, 0x90, 0x90, 0xe0, // D
        0xf0, 0x80, 0xf0, 0x80, 0xf0, // E
        0xf0, 0x80, 0xf0, 0x80, 0x80  // F
    };

    class Emulator
    {
    public:
        // *structors
        Emulator() = delete;
        Emulator(const char *program);
        ~Emulator() = default;
        Emulator(const Emulator &other) = delete;
        Emulator(Emulator &&other) noexcept = delete;
        Emulator &operator=(const Emulator &other) = delete;
        Emulator &operator=(Emulator &&other) noexcept = delete;

        // Methods
        Byte *get_keypad();
        const SharedDWordArrPtr get_frame_buf() const;
        void execute();

        // Chip instructions
        void _00e0();
        void _00ee();
        void _1nnn(Word addr);
        void _2nnn(Word addr);
        void _3xkk(Byte reg_x, Byte byte);
        void _4xkk(Byte reg_x, Byte byte);
        void _5xy0(Byte reg_x, Byte reg_y);
        void _6xkk(Byte reg_x, Byte byte);
        void _7xkk(Byte reg_x, Byte byte);
        void _8xy0(Byte reg_x, Byte reg_y);
        void _8xy1(Byte reg_x, Byte reg_y);
        void _8xy2(Byte reg_x, Byte reg_y);
        void _8xy3(Byte reg_x, Byte reg_y);
        void _8xy4(Byte reg_x, Byte reg_y);
        void _8xy5(Byte reg_x, Byte reg_y);
        void _8xy6(Byte reg_x);
        void _8xy7(Byte reg_x, Byte reg_y);
        void _8xye(Byte reg_x);
        void _9xy0(Byte reg_x, Byte reg_y);
        void _annn(Word addr);
        void _bnnn(Word addr);
        void _cxkk(Byte reg_x, Byte byte);
        void _dxyn(Byte reg_x, Byte reg_y, Byte byte);
        void _ex9e(Byte reg_x);
        void _exa1(Byte reg_x);
        void _fx07(Byte reg_x);
        void _fx0a(Byte reg_x);
        void _fx15(Byte reg_x);
        void _fx18(Byte reg_x);
        void _fx1e(Byte reg_x);
        void _fx29(Byte reg_x);
        void _fx33(Byte reg_x);
        void _fx55(Byte reg_x);
        void _fx65(Byte reg_x);

    private:
        const Word fetch() const;

        Byte regs[REGISTER_SIZE];
        Word opcode;
        UniqueByteArrPtr memory;
        Word idx;
        Word pc;
        SharedDWordArrPtr frame_buf;
        Byte delay_timer;
        Byte sound_timer;
        Word stack[STACK_SIZE];
        Word sp;
        Byte keypad[KEYPAD_SIZE];
    };
}
