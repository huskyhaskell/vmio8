#pragma once

#include <cstdarg>
#include <cstdio>
#include <cstdlib>

#define TODO(fmt, ...) { fprintf(stderr, "TODO: " fmt, __VA_ARGS__); exit(1); }