#pragma once

#include <SDL2/SDL.h>
#include "emulator.hpp"

namespace Vmio8
{
    class Platform
    {
    public:
        Platform() = delete;
        Platform(const char *title,
                 int window_width,
                 int window_height,
                 int texture_width,
                 int texture_height);
        ~Platform();
        Platform(const Platform &other) = delete;
        Platform(Platform &&other) noexcept = delete;
        Platform& operator=(const Platform &other) = delete;
        Platform& operator=(Platform &&other) noexcept = delete;

        bool process_input(Byte *keypad) const;
        void update(SharedDWordArrPtr frame_buf, int pitch);

    private:
        SDL_Window *window;
        SDL_Renderer *renderer;
        SDL_Texture *texture;
    };
}