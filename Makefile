CC = g++ -std=c++17 -Wall -Werror
CCDEBUG = $(CC) -g -fsanitize=address -static-libasan
CCRELEASE = $(CC) -O2

release: emulator.o platform.o main.cpp
	$(CCRELEASE) emulator.o platform.o main.cpp -o vmio8 -lSDL2

debug: emulator.o platform.o main.cpp
	$(CCDEBUG) emulator.o platform.o main.cpp -o vmio8 -lSDL2

platform.o: platform.cpp platform.hpp
	$(CC) -c platform.cpp

emulator.o: emulator.cpp emulator.hpp
	$(CC) -c emulator.cpp

clean:
	rm emulator.o platform.o vmio8
