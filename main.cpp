#include "emulator.hpp"
#include "platform.hpp"
#include <cstdio>
#include <stdexcept>
#include <chrono>

using namespace Vmio8;
using namespace std::chrono;

int main(int argc, char **argv)
{
    try
    {
        if (argc < 4)
        {
            throw std::runtime_error{"Usage: ./vmio8 [SCALE] [DELAY] [ROMFILE]\n"};
        }

        auto video_scale{std::stoi(argv[1])};
        auto cycle_delay{std::stoi(argv[2])};
        auto *rom_filename{argv[3]};
        Platform platform{"VMIO8",
                          FRAME_WIDTH * video_scale,
                          FRAME_HEIGHT * video_scale,
                          FRAME_WIDTH,
                          FRAME_HEIGHT};
        Emulator emulator{rom_filename};

        auto video_pitch{sizeof(DWord) * FRAME_WIDTH};
        auto last_cycle_time{high_resolution_clock::now()};
        _V2::system_clock::time_point current_time{};
        float delay_time{};

        auto quit{false};

        while (!quit)
        {
            quit = platform.process_input(emulator.get_keypad());

            current_time = high_resolution_clock::now();
            delay_time = duration<float, milliseconds::period>(current_time - last_cycle_time).count();
            if (delay_time > cycle_delay)
            {
                last_cycle_time = current_time;
                emulator.execute();
                platform.update(emulator.get_frame_buf(), video_pitch);
            }
        }
    }
    catch (const std::runtime_error &e)
    {
        printf("%s\n", e.what());
        return 1;
    }
    return 0;
}
